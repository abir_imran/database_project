-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2018 at 10:35 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `h64`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_h64`
--

CREATE TABLE `tbl_h64` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(24) NOT NULL,
  `Gender` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_h64`
--

INSERT INTO `tbl_h64` (`id`, `Name`, `Email`, `Gender`) VALUES
(2, 'Sadik', 'sadik@gmail.com', 'Male'),
(3, 'abir imran', 'abir@gmail.com', 'Male'),
(5, 'abir', 'abir@gmail.com', 'Male'),
(6, 'abir', 'abir@gmail.com', 'Male'),
(7, 'abir', 'abir@gmail.com', 'Male'),
(8, 'abir', 'abir@gmail.com', 'Male'),
(9, 'abir', 'abir@gmail.com', 'Male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_h64`
--
ALTER TABLE `tbl_h64`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_h64`
--
ALTER TABLE `tbl_h64`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
